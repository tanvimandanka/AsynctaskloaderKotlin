package com.asynctaskloader

import android.app.LoaderManager
import android.content.AsyncTaskLoader
import android.content.Loader
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast


class MainActivity : AppCompatActivity(),LoaderManager.LoaderCallbacks<String> {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        loaderManager.initLoader(1,null,this)
    }

    override fun onCreateLoader(p0: Int, p1: Bundle?): Loader<String> {
        // perform background operation then return result
        return object : AsyncTaskLoader<String>(this){
            override fun loadInBackground(): String {
                for (i in 0..100){
                Thread.sleep(50)
                    Log.d("---","loadInBackgroud:"+i)
                }
                return "Hey there! I am from asyntaskloader"
            }

            override fun onStartLoading() {
                forceLoad()
            }
        }
    }

    override fun onLoadFinished(p0: Loader<String>?, p1: String?) {
       Toast.makeText(this,p1,Toast.LENGTH_SHORT).show()
    }

    override fun onLoaderReset(p0: Loader<String>?) {
       Toast.makeText(this,"task has been reset",Toast.LENGTH_SHORT).show()
    }
}
